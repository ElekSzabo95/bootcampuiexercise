/**
 * JS function to simulate type writing. It takes the content from the html tag
 * identified by the sourceSelector (used as a CSS selector). Deletes and
 * rewrites it imitating a typewriter. The source tag has to be hidden at the
 * start using a css class 'hidden' It copies over inner tags in one level
 * depth. Cascaded inner tags deeper than one level are not supported. Typing is
 * timed using the setTimeout function as a callback for this function and so it 
 * goes through the letters one by one.
 * 
 * @author Elek Szabo
 */

"use strict";

function setupTyping(sourceSelector, callbackFunction) {
	var HtmlToType = $(sourceSelector).html(); // save the content to type
	$(sourceSelector).empty(); // clear the content of the tag so it can by
								// typed back later
	$(sourceSelector).removeClass('hidden'); // showing the tag so typing
												// will be visible
	
	var cursorPosition = 0, 
	
	readingEscape = false, //true means we are processing an escape character (&...;)
	
	readingTag = false, // true means we are processing
						// an inner tag
	closingTag = false, // true means we are processing the close of the last
						// opened inner tag
	withinTag = false, // true mean we are within an inner tag (eg between <i>
						// and </i>)
	tagContent = '', // the content of tag being processed (the part '<...>')
	
	escapeContent = ''; // the content of the escape character being processed
	

	var typing = function() {
		var nextLetter = HtmlToType[cursorPosition];

		switch (nextLetter) {
		case '&':
			if (!readingTag) { //switch to reading escape character mode but only if we are not reading a tag
				readingEscape = true;
				escapeContent = escapeContent + nextLetter;
			} else { //reading tag mode
				tagContent = tagContent + nextLetter;
			}
			break;
		case ';':
			if (readingEscape) { //close reading escape character mode
				escapeContent = escapeContent + nextLetter;
				$(sourceSelector).append(escapeContent);
				readingEscape = false;
				escapeContent = '';
			} else { //not in reading escape mode
				if (readingTag) {
					tagContent = tagContent + nextLetter;
				} else {
					$(sourceSelector).append(nextLetter);
				}
			}
			break;
		case '<': //start reading tag mode
			readingTag = true;
			tagContent = tagContent + nextLetter;
			break;
		case '/':
			if (HtmlToType[cursorPosition - 1] === '<') { //closing tag
				closingTag = true;
				tagContent = tagContent + nextLetter;
			} else {
				if(readingTag) {
					tagContent = tagContent + nextLetter;
				} else {
					$(sourceSelector).append(nextLetter);
				}
			}
			break;
		case '>':
			if (readingTag) { //close reading tag mode
				tagContent = tagContent + nextLetter;
				if (closingTag) { // closing of a close tag, move one level higher
					withinTag = false;
				} else { // closing of an open tag, write it out and move one
							// level deeper
					$(sourceSelector).append(tagContent);
					if (tagContent !== '<br>') {
						withinTag = true;
					}
				}
				tagContent = '';
				readingTag = false;
				closingTag = false;
			} else { //not in reading tag mode
				$(sourceSelector).append(nextLetter);
			}
			break;
		default:
			if (readingTag) {
				tagContent = tagContent + nextLetter;
			} else if(readingEscape) {
				escapeContent = escapeContent + nextLetter;
			} else if(withinTag) { //append to the cascaded tag
				$(sourceSelector + ' :last-child').append(nextLetter);
			} else {
				$(sourceSelector).append(nextLetter);
			}
			break;
		}

		cursorPosition += 1;

		if (cursorPosition < HtmlToType.length) {
			if (readingTag || readingEscape) { //tags and escapes are written out without delay
				setTimeout(typing, 0);
			} else {
				setTimeout(typing, 40 + (Math.random() * 80));
			}
		} else { //reached the end of the content to type
			callbackFunction();
		}
	}

	return {
		typing : typing
	};
}