/**
 * Set of functions to imitate a command line interface.
 * The following content should be prepared in the html to make this work:
 * - Inital content with id 'totype' and class 'hidden'
 * - Each commands marked with class 'command' and proper onclick attribute
 * - A cursor with id 'cursor' at the end of the initial content
 * - An element with id 'input_output' reserved for the user input / system output dialogs
 * - A prepared hidden 'dialog' element with a separate 'user_input' and 'system_output' elements
 * - A prepared hidden 'errormessage'
 * - A prepared hidden 'confirmmessage'
 * - A prepared hidden 'helpmessage'
 * 
 * @author Elek Szabo
 */

"use strict";

(function() {

/**
 * Keeping track if an input field is in focus or not.
 * Key events are only handled if input fields are not in focus
 */
var inputInFocus = false;

/**
 * Keeping track if an input field is in focus or not.
 * Key events are only handled if input fields are not in focus
 */
$('input').focusin(function () {
	inputInFocus = true;
});

/**
 * Keeping track if an input field is in focus or not.
 * Key events are only handled if input fields are not in focus
 */
$('input').focusout(function () {
	inputInFocus = false;
});

/**
 * Typing the initial content of the page.
 * This content has to be prepared and set with id 'totype'.
 */
$(document).ready(function() {
	setupTyping('#totype', addDialog).typing(); //type main content and once ready callback to add initial dialog
});

/**
 *  Blinking cursor
 *  This content has to be prepared and set with id 'cursor'.
 */
setInterval(function() {
    $( "#cursor" ).fadeToggle();
}, 200);

/**
 *  Helper function to add a new input/output pair dialog as the next line. 
 *  The dialog has to be prepared and set with id 'dialog'.
 *  Within the dialog there has to be a separate user_input and system_output part with respective ids
 *  The dialog is inserted in the element with id 'input_output'
 */
function addDialog() {
	//remove user_input and system_output ids from the previous dialog
	$('#input_output #user_input').removeAttr('id');
	$('#input_output #system_output').removeAttr('id');
	//add new dialog
	$('#input_output').append($('#dialog').html()); 
};

/**
 * Handling user input.
 * Parsing input in case of Enter.
 * Appending the pressed key as the next character in every other case.
 */
$(document).keypress(function(event) {
	if (!inputInFocus) {
		if (event.keyCode == 13) {//Enter > parse input
			var input = $('#user_input').text();
			$('#user_input').append('<br>'); //adding line break, so system output goes to next line
			if (input == '?') { //show help message
				var helpmessage = $('#helpmessage').html();
				$('.command').each(function() { //append the available commands to the help message
					helpmessage = helpmessage + ', ' + $(this).text();
				});
				$('#system_output').append(helpmessage).append('<br>');
				setupTyping('#system_output', addDialog).typing(); //type it out and add new dialog as callback
			} else { 
				//check for a command
				var executingCommand = false;
				$('.command').each(function() { //comparing input with the available commands
					if ($(this).text() == input) { 
						//show confirm message
						$('#system_output').append($('#confirmmessage').html()).append('<br>');
						//let it type and fire a click on the anchor and add a dialog in the callback 
						var anchor = $(this);
						setupTyping('#system_output', function() {
							addDialog();
							anchor.click();
							}).typing();
						executingCommand = true;
					};
				});
				if (!executingCommand) { //Enter pressed, but invalid input (neither '?', nor any command)
					//show error message
					$('#system_output').append($('#errormessage').html()).append('<br>');
					setupTyping('#system_output', addDialog).typing();
				}
			}			
		} else { //append any other character 
			$('#user_input').append(event.key);
		};
	};
});

/**
 * Handling backspace.
 * Backspace does not generate a keypress event so it has to be handled as keydown
 */
$(document).keydown(function(event) {
	if (!inputInFocus) {
		if (event.keyCode == 8) { //backspace
			$('#user_input').html( $('#user_input').html().slice(0,-1) );
		};
	};
});

})();